/**
 * @file
 * File to clear dates and also to display the selected dates.
 */

 jQuery(document).ready(
     function ($) {
            jQuery("a.clear-dates").on(
                "click", function (e) {
                    var link = this;

                    e.preventDefault();

                    jQuery("<div>Are you sure you want to clear the dates?</div>").dialog(
                        {
                            buttons: {
                                "Ok": function () {
                                    window.location = link.href;
                                },
                                "Cancel": function () {
                                    $(this).dialog("close");
                                }
                            }
                        }
                    );

                }
               );

               // Implementation of UL LI for the display of selected dates.
            if ((Drupal.settings.calendar_multiselect != null && Drupal.settings.calendar_multiselect.length > 1)) {
                var lashHoldiaysArray = JSON.parse(Drupal.settings.calendar_multiselect);
                var natDays = lashHoldiaysArray;

                var previous = new Array();

                for (i = 0; i < natDays.length; ++i) {
                    natDays[i][0] = natDays[i][0].replace(/\s+/g, '');
                    natDays[i][1] = natDays[i][1].replace(/\s+/g, '');
                    natDays[i][2] = natDays[i][2].replace(/\s+/g, '');
                    previous.push(natDays[i][0] + "/" + natDays[i][1] + "/" + natDays[i][2]);
                }

              var previous_show_dates_li = '';
              previous_show_dates_li = arrange_dates(previous);

              jQuery("#show_dates").html(previous_show_dates_li);
            }
                  var year = (new Date).getFullYear();
                  var date = new Date();
            if (jQuery("#multi-date-field").length != 0) {
                jQuery('#multi-date-field').multiDatesPicker(
                  {
                        minDate: new Date(year, 0, 1), // Today.
                        maxDate: new Date(year + 1, 0, 31),
                        altField: '#custom_date_pick',
                        addDates: previous,
                        onSelect: function (dateText, inst) {
                            var selected_dates = jQuery("#custom_date_pick").val();
                            jQuery("#show_dates_ul").html('');
                            var selected_dates = selected_dates.replace(/\s/g, "");
                            var selected_dates_arr = selected_dates.split(',');
                            var show_dates_li = arrange_dates(selected_dates_arr);

                            jQuery("#show_dates").html(show_dates_li);
                        },
                          }
           );
            }

                     jQuery('.form-item-custom-date-popup-date').hide();

        }
 );

 function arrange_dates(selected_dates_arr) {
     var html = '<ul><li id="show_dates_ul">';
     var show_dates_li = '';
     for (j = 0; j < selected_dates_arr.length; j++) {
         if (j == 0) {
             show_dates_li += '<ul>';
             show_dates_li += '<li>' + selected_dates_arr[j] + '</li>';
            }
            else {
                if (j + 1 == selected_dates_arr.length) {
                    show_dates_li += '<li>' + selected_dates_arr[j] + '</li></ul>';
                }
                else {
                    if ((j + 1) % 4 == 0) {
                        show_dates_li += '<li>' + selected_dates_arr[j] + '</li></ul><ul>';
                    }
                    else {
                        show_dates_li += '<li>' + selected_dates_arr[j] + '</li>';
                    }
                }
            }
        }
        if (selected_dates_arr[0] == '') {
            show_dates_li = 'No dates selected';
        }
        html += show_dates_li;
        html += '</li></ul>';
        return html;
    }
