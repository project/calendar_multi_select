=====
Calendar Multiselect
https://www.drupal.org/project/calendar_multi_select
-----
Calendar multiselect has been developed and maintained by Danish.
https://www.drupal.org/u/danisha

=====
Installation
-----

1. Enable the module and its dependent modules
2. Make sure that the field "Alternate jQuery version for administrative pages"
has the value "Default jQuery Version" selected on the jquery configuration page
at admin/config/development/jquery_update.
3. Make sure the permission named "Administer Multiselect Calendar" is enabled
for the role which needs to view the configuration page.
4. Goto the multiselect configuration page at 
admin/config/system/multiselect_cal